//
//  AppDelegate.h
//  Connect4
//
//  Created by SnowBiscuit on 4/18/14.
//  Copyright (c) 2014 SnowBiscuit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
