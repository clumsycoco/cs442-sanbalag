//
//  BoardView.m
//  Connect4
//
//  Created by SnowBiscuit on 4/18/14.
//  Copyright (c) 2014 SnowBiscuit. All rights reserved.
//

#import "BoardView.h"


@implementation BoardView
{
    NSMutableArray *_columnViews;
}

-(void)layoutSUbviews
{
    for(int i=0;i<7;i++)
    {
        UIView *columnView = [[UIView alloc] intitWithFrame:CGRectMake(_gridWidth*i-12.5, 0, 25, self.bounds.size.height)];
        columnView.backgroundColor = [[UIColor redColor]];
        
        UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(columnTapped:)];
        tap.numberOfTapsRequired =1 ;
        tap.numberOfTouchesRequired = 1;
        
        [columnView addGestureRecognizer:tap];
        
        [self addSubview:columnView];
        
    }
}

-(void)columnTapped:(UIGestureRecognizer *)gestureRecognizer
{
    NSLog(@"column tapped!");
}

-(void)drawRect:(CGRect)rect
{
 
    _gridHeight = self.bounds.size.height/7;
    _gridwidth = self.bounds.size.width/8;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillcolorWithColor(context,[UIColor redcolor].CGColor);
    CGContextAddRect(context, self.bounds);
    
    
    for(int i=1;i<=7;i++)
    {
        for(int j=1;j<=6;j++)
        {
            CGContextAddRect(context,CGRectMake(i*_gridwidth-5, j*_gridHeight-5,, 10));
        }
    }
    CGContextEOFillPath(context);
}


@end
