//
//  ViewController.h
//  Connect4
//
//  Created by SnowBiscuit on 4/18/14.
//  Copyright (c) 2014 SnowBiscuit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BoardView.h"
@interface ViewController : UIViewController
@property(weak,nonatomic)IBOutlet BoardView *boardView;
@end


