//
//  ViewController.m
//  unitConvertorMp1
//
//  Created by SnowBiscuit on 3/11/14.
//  Copyright (c) 2014 SnowBiscuit. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

bool buttonToggled = NO;
double direction = 1;
double factor;
double factorUp;
double factorDown;
- (void)viewDidLoad
{
    [super viewDidLoad];
	//self.topField.text = @"Metric Unit";
    //self.botField.text = @"Imperial Unit";
    self.topField.text = @"Cº";
    self.botField.text = @"Fº";
    
}



- (IBAction)convertClicked:(id)sender
{
    [self display];
    
}

- (IBAction)swap:(id)sender
{
    if (!buttonToggled) {
        [sender setTitle:@"▼" forState:UIControlStateNormal];
        buttonToggled = YES;
        direction = 0;
    }
    else {
        [sender setTitle:@"▲" forState:UIControlStateNormal];
        buttonToggled = NO;
        direction = 1;
    }
}

-(void)display
{
    double d ;
    
    
    if(!direction)
    {
        if(self.segment.selectedSegmentIndex!=0)
        {
        factor = factorDown;
        d= factor*[self.topValue.text doubleValue];
      
         self.botValue.text = [NSString stringWithFormat:@"%.2f",d];
        }
        else
        {
            d= [self.topValue.text doubleValue];
            self.botValue.text = [NSString stringWithFormat:@"%.2f",(d*9/5)+32];
        }
    }
    else
    {
        if(self.segment.selectedSegmentIndex!=0)
        {
            factor =factorUp;
        d =factor*[self.botValue.text doubleValue];
      
        self.topValue.text =[NSString stringWithFormat:@"%.2f",d];
        }
        else
        {
            d =[self.botValue.text doubleValue];
            
            self.topValue.text =[NSString stringWithFormat:@"%.2f",(d-32)*5/9];
        }
    }
}
- (IBAction)segmentClicked:(id)sender
{
    switch (self.segment.selectedSegmentIndex)
    {
        case 0:  self.topField.text =@"Cº";
            self.botField.text= @"Fº";
            [self display];
            
            break;
        case 1 : self.topField.text =@"Kilometers";
            self.botField.text = @"Miles";
            factorUp = 1.60934;
            factorDown=0.621371;
            [self display];
            break;
        case 2 : self.topField.text =@"Litres";
            self.botField.text =@"Gallons";
            factorUp=3.78541;
            factorDown = 0.264172;
            [self display];
            break;
        default:
            break;
    }
    
}

- (IBAction)vanish:(UITapGestureRecognizer *)sender
{
    [self.topValue resignFirstResponder];
    [self.botValue resignFirstResponder];
}
- (IBAction)topSelected:(UITextField *)sender
{
    
        [self.toggle setTitle:@"▲" forState:UIControlStateNormal];
    
    direction = 1;
    
}

- (IBAction)botSelected:(UITextField *)sender {
    [self.toggle setTitle:@"▼" forState:UIControlStateNormal];
    
    direction = 0;
}
@end
