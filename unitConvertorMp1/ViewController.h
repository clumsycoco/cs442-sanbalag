//
//  ViewController.h
//  unitConvertorMp1
//
//  Created by SnowBiscuit on 3/11/14.
//  Copyright (c) 2014 SnowBiscuit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *topValue;
@property (weak, nonatomic) IBOutlet UITextField *botValue;
- (IBAction)topSelected:(UITextField *)sender;
- (IBAction)botSelected:(UITextField *)sender;
@property (weak, nonatomic) IBOutlet UIButton *toggle;
@property (weak, nonatomic) IBOutlet UILabel *topField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
@property (weak, nonatomic) IBOutlet UILabel *botField;
- (IBAction)convertClicked:(id)sender;
- (IBAction)swap:(id)sender;
- (IBAction)segmentClicked:(id)sender;
- (IBAction)vanish:(UITapGestureRecognizer *)sender;

@end
