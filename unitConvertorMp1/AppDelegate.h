//
//  AppDelegate.h
//  unitConvertorMp1
//
//  Created by SnowBiscuit on 3/11/14.
//  Copyright (c) 2014 SnowBiscuit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
